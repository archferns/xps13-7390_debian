Debian 10 on Dell XPS 13 7390 2-in-1 (2019)
===========================================

This repository contains the scripts and process for getting a (mostly) 
working Debian Buster/Bullseye installation on the new Dell XPS 13 2-in-1. 
Much of this was taken from partipating in [this Reddit thread](https://www.reddit.com/r/Dell/comments/cx0fkc/xps_13_2_in_1_7390_linux_boot_attempt/).

### Not Working

* Webcam (no fix known yet)

## Setup

You'll need to boot from a kernel >=v5.3. If you are using a Debian 10 image 
that probably means you are on v5.2. So press `e` at the GRUB menu, find the 
line that starts with `linux` and append `module_blacklist="intel_lpss_pci"` 
before proceeding to boot.

Once you have your base system installed, boot again using that same flag. 
You'll want to find an elternate means for connecting to the internet since 
your wireless will not work yet. I used an Android phone's USB tethering 
fuctionality.

> If you are using Ubuntu and have Secure Boot enabled, you will need to
> disable it.

Next, clone this repository which contains what you need to:

* Download the mainline kernel (v5.4 at time of writing)
* Apply patches for booting without blacklisting LPSS, fix the touchscreen, + other
* Compile the custom kernel with the appropriate config
* Backport the wireless drivers and fix bluetooth

First upgrade your system and install some tools.

```
sudo apt update
sudo apt upgrade
sudo apt install git build-essential flex bison libssl-dev libelf-dev bc 
# optionally (if prefer to build kernel deb)
sudo apt install kernel-package fakeroot
```

Get this repository.

```
git clone https://gitlab.com/emrose/xps13-7390_debian
cd xps13-7390_debian
```

Run the scripts.

```
./opt/scripts/build-custom-kernel.sh
./opt/scripts/strip-kernel-modules.sh
./opt/scripts/update-initramfs.sh
./opt/scripts/fix-wifi-and-bluetooth.sh
```

For better video performance and OpenGL support, you may wish to optionally fall back on the modesetting driver:

```
sudo apt-get remove xserver-xorg-video-intel
```

Reboot with fingers crossed.

```
sudo reboot
```


Suspend/Hibernation
-------------------

As of BIOS version 1.0.13, it is possible to enable ACPI S3 in the BIOS settings,
however the built-in display remains corrupted after resume. (#3) In Linux, the system
defaults to ACPI S0ix ("connected standby"), however Linux has limited software support
for this mode (i.e. doesn't take much advantage of the fact that the CPU is
still running). Unfortunately, S0ix still uses far more power than S3 or similar modes.
Systemd's "hybrid suspend" (S0ix for a limited time, then suspend to disk) remains as
a reasonable compromise between resume time and power consumption.

To enable it, follow these steps:

*   Make sure you have a swap partition or swap file, and specify it
    on the kernel commandline `resume=DEVICE` parameter. If you have
    a swap file, you will also need the `resume_offset=INTEGER` parameter.
    Swap files on encrypted root partitions work fine.

    As soon as you do this and reboot, `systemctl hibernate` should
    hibernate your machine.

*   Edit `/etc/systemd/sleep.conf` to include the setting
    ```
    [Sleep]
    # The default, "platform", hibernates in ACPI S4, which still uses considerable power.
    HibernateMode=shutdown
    # Or some other time period that you find works well:
    HibernateDelaySec=45min
    ```
    As soon as you do this and reboot, `systemctl suspend-then-hibernate` should
    operate with the desired delay, which you can verify by using a short delay
    (one minute?) and experimenting.
    Hibernate should also not drain the battery.

*   Edit `/etc/systemd/logind.conf` to include the setting
    ```
    [Login]
    HandleLidSwitch=suspend-then-hibernate
    ```
*   To enable triggering hibernation as a normal user, create 
    `/etc/polkit-1/localauthority/50-local.d/com.ubuntu.enable-hibernate.pkla` to contain
    ```
    [Re-enable hibernate by default in upower]
    Identity=unix-user:*
    Action=org.freedesktop.upower.hibernate
    ResultActive=yes

    [Re-enable hibernate by default in logind]
    Identity=unix-user:*
    Action=org.freedesktop.login1.hibernate;org.freedesktop.login1.handle-hibernate-key;org.freedesktop.login1;org.freedesktop.login1.hibernate-multiple-sessions;org.freedesktop.login1.hibernate-ignore-inhibit
    ResultActive=yes
    ```
    [Source](https://askubuntu.com/a/819891)
*   Finally, if you use Gnome Shell, you can use [this shell extension](https://extensions.gnome.org/extension/755/hibernate-status-button/)
    to add a hibernation button to the main shell menu.

Notes
-----

* Some users have reported issues with some HDMI dongles, if that's you [there is a workaround](https://gitlab.com/emrose/xps13-7390_debian/issues/4)
