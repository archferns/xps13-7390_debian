#!/bin/sh

if [ -d "/lib/modules/5.4.0" ]; then
  cd /lib/modules/5.4.0
fi

if [ -d "/lib/modules/5.4.0+" ]; then
  cd /lib/modules/5.4.0+
fi

sudo find . -name *.ko -exec strip --strip-unneeded {} +
